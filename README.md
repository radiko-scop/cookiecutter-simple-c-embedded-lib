# CookieCutter - Simple C embedded lib

This is a cookie cutter template, for a c library ready to be built as part of an embeded project.

## Usage example

### Create project skeletton

- `cookiecutter https://gitlab.com/radiko-scop/cookiecutter-simple-c-embedded-lib.git`
- or, if private repo `cookiecutter git@gitlab.com:radiko-scop/cookiecutter-simple-c-embedded-lib.git`

### Using generated code

- Define your hardware layer abstraction if needed
- import your lib as a part of the project that contains hardware information (board support package)