#include <stdarg.h>
#include <stdio.h>
#include <{{cookiecutter.package_name}}/log.h>
#include <{{cookiecutter.package_name}}/hal/com.h>

#define LOG_ENTRY_SIZE 512 // max size for one log line
static char m_log_entry[LOG_ENTRY_SIZE];
static log_level m_log_level = DEBUG;
const char* m_log_prefix[] = {"debug", "info", "warning", "error"};

void log_set_level(log_level debug_level)
{
    m_log_level = debug_level;
}

static void log_print(log_level level, const char *fmt, va_list args)
{
    if (level >= m_log_level)
    {
        vsnprintf(m_log_entry, LOG_ENTRY_SIZE, fmt, args);
        com_printf("%s: %s\r\n", m_log_prefix[level], m_log_entry);
    }
}

void log_info(const char *fmt, ...)
{
    va_list arglist;
    va_start(arglist,fmt);
    log_print(INFO, fmt, arglist);
    va_end(arglist);
}

void log_debug(const char *fmt, ...)
{
    va_list arglist;
    va_start(arglist,fmt);
    log_print(DEBUG, fmt, arglist);
    va_end(arglist);
}

void log_warning(const char *fmt, ...)
{
    va_list arglist;
    va_start(arglist,fmt);
    log_print(WARNING, fmt, arglist);
    va_end(arglist);
}

void log_error(const char *fmt, ...)
{
    va_list arglist;
    va_start(arglist,fmt);
    log_print(ERROR, fmt, arglist);
    va_end(arglist);
}
