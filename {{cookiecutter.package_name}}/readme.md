# {{cookiecutter.package_name}}

## About

{{cookiecutter.description}}
{{cookiecutter.author}}
{{cookiecutter.email}}

## Test

```bash
mkdir build && cd build
cmake -DENABLE_TESTS=ON ..
make
./tests/{{cookiecutter.package_name}}-test
```

You should see something like:

```bash
[==========] Running 0 tests from 0 test suites.
[==========] 0 tests from 0 test suites ran. (0 ms total)
[  PASSED  ] 0 tests.
```

## Build