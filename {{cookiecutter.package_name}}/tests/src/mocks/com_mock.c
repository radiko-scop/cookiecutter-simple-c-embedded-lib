#include <stdio.h>
#include <stdarg.h>
#include <{{cookiecutter.package_name}}/hal/com.h>

// Mocks for the com functions of HAL

int com_printf(const char* format, ...)
{
  va_list arglist;
  va_start(arglist, format);
  vprintf(format, arglist);
  va_end(arglist);
}
