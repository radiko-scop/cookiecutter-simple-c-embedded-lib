#ifndef TEST_{{cookiecutter.package_name}}_H
#define TEST_{{cookiecutter.package_name}}_H

#include "gtest/gtest.h"

class Test{{cookiecutter.package_name}} :  public ::testing::Test
{
public:
    explicit Test{{cookiecutter.package_name}}();
    virtual ~Test{{cookiecutter.package_name}}();
private:

protected:
  virtual void SetUp();

  // virtual void TearDown() {}

};

#endif // TEST_{{cookiecutter.package_name}}_H