#include "Test{{cookiecutter.package_name}}.h"
#include <{{cookiecutter.package_name}}/{{cookiecutter.package_name}}.h>
#include <{{cookiecutter.package_name}}/log.h>
#include "mocks/mock.h"

Test{{cookiecutter.package_name}}::Test{{cookiecutter.package_name}}()
{

}

Test{{cookiecutter.package_name}}::~Test{{cookiecutter.package_name}}()
{

}

void Test{{cookiecutter.package_name}}::SetUp()
{
 log_info("Setting up tests ...");
}

TEST_F(Test{{cookiecutter.package_name}}, myDummyTest)
{
    EXPECT_EQ(1+1, 2);
}