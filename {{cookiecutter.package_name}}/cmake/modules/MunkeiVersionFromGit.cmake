# The MIT License (MIT)
#
# Copyright (c) 2016 Theo Willows
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

cmake_minimum_required( VERSION 3.0.0 )

include( CMakeParseArguments )

function( version_from_git )
  # Parse arguments
  set( options OPTIONAL FAST )
  set( oneValueArgs
    GIT_EXECUTABLE
    INCLUDE_HASH
    LOG
    TIMESTAMP
  )
  set( multiValueArgs )
  cmake_parse_arguments( ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  # Defaults
  if( NOT DEFINED ARG_INCLUDE_HASH )
    set( ARG_INCLUDE_HASH ON )
  endif()

  if( DEFINED ARG_GIT_EXECUTABLE )
    set( GIT_EXECUTABLE "${ARG_GIT_EXECUTABLE}" )
  else ()
    # Find Git or bail out
    find_package( Git )
    if( NOT GIT_FOUND )
      message( FATAL_ERROR "[MunkeiVersionFromGit] Git not found" )
    endif( NOT GIT_FOUND )
  endif()

  #Pollen Describe (--match v* to avoid tags not related to a version, as demos or so.)
  execute_process(
    COMMAND           "${GIT_EXECUTABLE}" describe --tags --match v* --dirty
    WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
    RESULT_VARIABLE   git_result
    OUTPUT_VARIABLE   git_describe_pollen
    ERROR_VARIABLE    git_error
    OUTPUT_STRIP_TRAILING_WHITESPACE
    ERROR_STRIP_TRAILING_WHITESPACE
  )
  if( NOT git_result EQUAL 0 )
    message( FATAL_ERROR
      "[MunkeiVersionFromGit] Failed to execute Git: ${git_error}"
    )
  endif()

  if( git_describe_pollen MATCHES "^v(0|[1-9][0-9]*)[-]?(0|[1-9][0-9]*)?(-[.0-9A-Za-z]+)?(-[.0-9A-Za-z-]+)?$" )
    set( pollen_version_major ${CMAKE_MATCH_1} )
    set( pollen_version_patch ${CMAKE_MATCH_2} )
    set( pollen_identifiers   "${CMAKE_MATCH_3}" )
    set( pollen_dirty      "${CMAKE_MATCH_4}" )
    message( STATUS "Result from ${CMAKE_CURRENT_SOURCE_DIR} :
    #  ${git_describe_pollen} (major=${pollen_version_major}, minor=${pollen_version_patch}, identifiers=${pollen_identifiers}, dirty=${pollen_dirty})")
  else()
    message( FATAL_ERROR
      "[MunkeiVersionFromGit] Git tag hasn't valid semantic pollen version: [${git_describe_pollen}]"
    )
  endif()

  if( pollen_version_patch STREQUAL "-dirty")
    set( pollen_version_patch 255 ) # 255 means dirty
  else()
    if( pollen_version_patch STREQUAL "")
      set( pollen_version_patch 0 )
    endif()
    set( pollen_version ${pollen_version_major}.${pollen_version_patch}${pollen_dirty} )
  endif()

  # Log the results
  if( ARG_LOG )
    message( STATUS
      "[MunkeiVersionFromGit] Version: ${version}
     PollenVersion: [${pollen_version}]
     Dirty:     [${pollen_dirty}]
     Git hash:    [${pollen_identifiers}]
     "
    )
  endif( ARG_LOG )

  # Set parent scope variables
  set( VERSION       ${pollen_version}       PARENT_SCOPE )
  set( VERSION_MAJOR       ${pollen_version_major}       PARENT_SCOPE )
  set( VERSION_PATCH       ${pollen_version_patch}       PARENT_SCOPE )

endfunction( version_from_git )
