#ifndef {{cookiecutter.package_name}}_LOGGER_HAL_H
#define {{cookiecutter.package_name}}_LOGGER_HAL_H

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus


// communication module used for logging. One need some uart or ... to display logs
// same module could be used later to communicate using uart with the board.

/**
 * @brief com_printf send a formatted string over com.
 * @param format : same as in printf
 * @return number of sent characters.
 */
int com_printf(const char* format, ...);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif //{{cookiecutter.package_name}}_LOGGER_HAL_H