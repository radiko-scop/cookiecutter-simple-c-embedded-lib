#ifndef {{cookiecutter.package_name}}_LOGGER_H
#define {{cookiecutter.package_name}}_LOGGER_H

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus


typedef enum {
    DEBUG=0, // Debug only
    INFO=1,
    WARNING=2,
    ERROR=3, // Error -> cancel current task
} log_level;

void log_set_level(log_level debug_level);
void log_info(const char *fmt, ...);
void log_debug(const char *fmt, ...);
void log_warning(const char *fmt, ...);
void log_error(const char *fmt, ...);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif //{{cookiecutter.package_name}}_LOGGER_H